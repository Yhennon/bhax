#include <stdio.h>

int main(){

	int a = 1;
	int b = 2;

	printf("A változók csere előtt: \n");
	printf("a = ");
	printf("%d\n",a);
	printf("b = ");
	printf("%d\n",b);

	a = a-b; // a most -1
	b = b+a; // b most 1
	a = b-a;// a most 2

	printf("A változók csere után: \n");
	printf("a = ");
	printf("%d\n",a);
	printf("b = ");
	printf("%d\n",b);
}