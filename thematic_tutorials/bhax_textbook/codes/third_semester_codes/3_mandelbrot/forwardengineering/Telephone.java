public class Telephone {
	private boolean _hook = true;
	private int _connection = 0;
	public Answering_Machine _unnamed_Answering_Machine_;
	public Caller_Id _unnamed_Caller_Id_;
	public Ringer _unnamed_Ringer_;

	public void onHook() {
		throw new UnsupportedOperationException();
	}

	public void offHook() {
		throw new UnsupportedOperationException();
	}

	public void dial(int aN) {
		throw new UnsupportedOperationException();
	}

	public void setCallerId(boolean aStatus) {
		throw new UnsupportedOperationException();
	}

	public void setAnsMachine(boolean aStatus) {
		throw new UnsupportedOperationException();
	}
}