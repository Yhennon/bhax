import java.util.Vector;

public class Line {
	private boolean _busy;
	public Vector<Telephone> _connectedPhones = new Vector<Telephone>();

	public void dial(int aN) {
		throw new UnsupportedOperationException();
	}

	public void offHook() {
		throw new UnsupportedOperationException();
	}

	public void onHook() {
		throw new UnsupportedOperationException();
	}
}