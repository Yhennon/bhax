#include <iostream>

class Mage{
public:
	void magic(){};
};

class ElementalMage : public Mage{ 
public:
	void elementalmagic(){};
};

int main(void){
	Mage* mage;
	ElementalMage* elementalmage; 
	elementalmage->magic();
	elementalmage->elementalmagic(); 
	mage->magic();
	//mage->elementalmagic();
};