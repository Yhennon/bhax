#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct binfa
{
  char ertek;
  struct binfa *bal_nulla;
  struct binfa *jobb_egy;
} BINFA, *BINFA_PTR;

BINFA_PTR uj_elem()
{
  BINFA_PTR p;

  if ((p = (BINFA_PTR) malloc (sizeof (BINFA))) == NULL)
    {
      perror ("memoria");
      exit (EXIT_FAILURE);
    }
  return p;
}

void kiir (BINFA_PTR elem); 
void szabadit (BINFA_PTR elem);

int main (int argc, char **argv)
{
  //initial declarations
  //===========================================
  char b;
  unsigned char c;
  BINFA_PTR gyoker = uj_elem ();
  gyoker->ertek = '/';
  gyoker->bal_nulla = gyoker->jobb_egy = NULL;
  BINFA_PTR fa = gyoker;
  //===========================================
  //===============================================================================
  //BEOLVASÁS
  //===============================================================================
while (read (0, (void *) &b, sizeof(unsigned char)))
    {
        for (int i = 0; i < 8; ++i) {

            if (b & 0x80)

                c='1';
            else
               c='0';
            b <<= 1;

    if (c == '0')
	{
	  if (fa->bal_nulla == NULL)
	    {
	      fa->bal_nulla = uj_elem ();
	      fa->bal_nulla->ertek = '0';
	      fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
	      fa = fa->bal_nulla;
	    }
	}
    else
		{
			if (fa->jobb_egy == NULL)
		    {
	      		  	fa->jobb_egy = uj_elem ();
	    		  	fa->jobb_egy->ertek = '1';
			      	fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
			      	fa = gyoker;
	  	  	}
	  		else
	    	{
	     	 	fa = fa->jobb_egy;
	    	}
		}
	}  
    }
//==================================================================================
  printf ("\n");
  kiir (gyoker);
  szabadit (gyoker);
}

//static int melyseg = 0;
int max_melyseg = 0, melyseg = 0;


//======================================================================================
//KIIR
//======================================================================================
void kiir(BINFA_PTR elem)
    {

        if (elem != NULL) {
            ++melyseg;
            kiir(elem->jobb_egy); //JOBB
            //===============================
            for (int i = 0; i < melyseg; ++i)
                printf("---");
            printf("%c\n",elem->ertek);
            //===============================
            kiir(elem->bal_nulla); //BAL
            --melyseg;
        }
    }
/*
preorder érték-jobb-bal
void kiir(BINFA_PTR elem)
    {

        if (elem != NULL) {
            ++melyseg;
     	    //===============================
            for (int i = 0; i < melyseg; ++i)
                printf("---");
            printf("%c\n",elem->ertek);
            //===============================
            kiir(elem->jobb_egy); //JOBB
            kiir(elem->bal_nulla); //BAL
            --melyseg;
        }
    }
*/
/*
posztorder bal-jobb-érték
void kiir(BINFA_PTR elem)
    {

        if (elem != NULL) {
            ++melyseg;
     	    
            kiir(elem->jobb_egy); //JOBB
            kiir(elem->bal_nulla); //BAL
	    //===============================
            for (int i = 0; i < melyseg; ++i)
                printf("---");
            printf("%c\n",elem->ertek);
            //===============================
            --melyseg;
        }
    }
*/

//======================================================================================
//szabaditás
//======================================================================================
void szabadit (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      szabadit (elem->jobb_egy);
      szabadit (elem->bal_nulla);
      free (elem);
    }
}
