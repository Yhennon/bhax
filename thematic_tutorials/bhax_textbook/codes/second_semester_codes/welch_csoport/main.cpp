#include <iostream>
#include <fstream>
#include <vector>
#include "binfa.h"

#define UJ_SOR 0x0a
#define N_BETU 0x4e
#define KACSACSOR 0x3e
#define MASZK 0x80

void usage(void)
{
    // szólunk a felhasználónak, hogy így kellett volna futtatni
    // a programot...
    std::cout << "Használat: ./z bemeneti_fájl -o kimeneti_fájl [-t]" << std::endl;
}

int main(int argc, char *argv[])
{
	std::vector<LZWBinFa> vektor;
	vektor.reserve(10);

	LZWBinFa F;
	F << '0' << '1' << '0';
	std::cout << "F.getGyoker() --> " << F.getGyoker() << std::endl;

	//LZWBinFa&& jobbF = std::move(F);
	std::cout << " -- -- LZWBinFa G = std::move(F);" << std::endl;

	// F-bő csinálok egy jobbértéket az std::move()-val
	// a bal érték lesz a G, a jobbérték pedig az std::move(F)

	LZWBinFa G = std::move(F);
	//G << '1';
	//G = std::move(F);

	//std::cout << "G.getGyoker() --> " << G.getGyoker() << std::endl;
	//std::cout << "F.getGyoker() --> " << F.getGyoker() << std::endl;
	
	std::cout << " -- -- vektor.push_back(std::move(F));" << std::endl;
	vektor.push_back(std::move(F));
	std::cout << std::endl << "vektor[0]" << std::endl << vektor[0] << std::endl;
	
	//std::cout << "F" << std::endl << F << std::endl;
	//std::cout << "G" << std::endl << G << std::endl;
	
	return 0;
}
/*
	/*
    //std::cout << *(*(argv)) << std::endl;
    
    // 0             1  2             3   4
    // z bemeneti_fájl -o kimeneti_fájl [-t]
    if (argc < 4 || argc > 5)
    {
    	std::cout << "argc teszt" << std::endl;
        usage();

        // hiba történt
        return -1;
    }

    // a második paraméter, azaz a bemeneti fájl nevének
    // eltárolása
    char *inFile = *++argv;

    // megnézem jó helyen van-e a -o kapcsoló, ha nem szólok, hogy baj van
    // a következő argumentum '-' -al kezdődik, ezért a *(X + 1), hogy csak
    // az o-t kelljen vizsgálni
    if (*((*++argv) + 1) != 'o')
    {
    	std::cout << "-o teszt" << std::endl;
        usage();

        // hiba történt
        return -2;
    }

    // a tényelges fájl beolvasása, az előbb elmentett fájlnévvel
    std::fstream beFile(inFile, std::ios_base::in);

    // kis hibakezelés
    if (!beFile)
    {
        std::cout << inFile << " nem letezik..." << std::endl;
        usage();

        // hiba történt
        return -3;
    }

    // a második paraméter, azaz a bemeneti fájl nevének
    // eltárolása
    char *outFile = *++argv;

    // a leendő kimeneti fájl létrehozása üresen a negyedik argumentumnak
    // megfelelő néven
    std::fstream kiFile(outFile, std::ios_base::out);

    // a másolást és efféle tesztelős módosítgatós részeket a kód végén
    // lévő if-be írom, ami csak akkor fut le, ha van -t kapcsoló futtatáskor
    bool teszt = false;

    if (argc == 5 && *((*++argv) + 1) != 't')
    {
    	std::cout << "-t teszt" << std::endl;
        usage();

        // hiba történt
        return -2;
    }
    else if (argc == 5)
    {
    	teszt = true;
    }

    // a z.c -ből jól ismert b, ebbe olvasom mindig az adott bájtját a bemenetnek
    unsigned char b;		

    // maga a bináris fa
    LZWBinFa binFa;		

    std::cout << "A bementi fájl feldolgozása... (" << inFile << ")" << std::endl;
    std::cout << "Ez akár több percet is igénybe vehet (mérettől függően)" << std::endl << std::endl;

    // az első sort átugrom
    while (beFile.read((char *) &b, sizeof(unsigned char)) && b != UJ_SOR)
        ;

    bool kommentben = false;

    // olvasom a bemenet bájtjait a következő képp:
    //
    //    HA kacsacsőr            -> kommentben vagyok megyek tovább
    //    HA új sor karakter      -> nem vagyok kommentben, de megyek tovább
    //    HA kommentben vagyok    -> megyek tovább
    //    HA az olvasott bájt 'N' -> megyek tovább
    //         
    //    KÜLÖNBEN
    //        Mivel bájtokat olvasok, ezen bájtok
    //        mind a 8 bitjén végig megyek egy
    //        maszk segítségével, majd ezt odaadom
    //        a fának, hogy kezdjen vele valamit
    //

    // "01111001001001000111";
    std::vector<char> v(20);
    v = {'0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '1', '0', '0', '1', '0', '0', '0', '1', '1', '1'};

    for (auto& betu : v)
    //for (std::vector<char>::iterator it = v.begin(); it != v.end(); ++it)
    //while (beFile.read((char *) &b, sizeof(unsigned char)))
    {
        b = betu;
        //b = *it;

        if (b == KACSACSOR)
        {			
            kommentben = true;
            continue;
        }

        if (b == UJ_SOR)
        {			
            kommentben = false;
            continue;
        }

        if (kommentben)
        {
            continue;
        }

        if (b == N_BETU)		
        {
            continue;
        }

        binFa << b;

        // a már jól ismert d.c -ben található maszkolása az adott bájt-nak:
        // mindig kiéselem a bájt legfelső helyiértéken levő bitjét, majd
        // ezt shiftelem bele a fába
        /*
        for (int i = 0; i < 8; ++i)
        {
            if (b & MASZK)
            {
                // ha 1 a bit
                binFa << '1';
            }
            else
            {
                // ha 0 a bit
                binFa << '0';
            }

            // tolom a bitjeit az alap bájt-nak, hogy a 2^(n-1)-es helyiértékű bit
            // a 2^n-es helyiértéken legyen
            b <<= 1;
        }
        
    }

    // binfa kiírása fájlba
    kiFile << binFa;

    kiFile << "melyseg  = " << binFa.getMelyseg()  << std::endl; // mélység
    kiFile << "atlag    = " << binFa.getAtlag()    << std::endl; // átlag
    kiFile << "szoras   = " << binFa.getSzoras()   << std::endl; // szórás
    kiFile << "egyesek  = " << binFa.getEgyesek()  << std::endl; // egyesek száma a fában
    kiFile << "nullasok = " << binFa.getNullasok() << std::endl; // nullások száma a fában

    // bezárom ha megnyitottam
    kiFile.close();
    beFile.close();

    std::cout << "A kimeneti fájl elkészült! ("<< outFile << ")" << std::endl;
    std::cout << "Az ellenőrzéshez: tail " << outFile << std::endl;

    if (teszt)
    {
        LZWBinFa f1;
        f1 << '0' << '0';  
        std::cout << "f1" << std::endl << f1 << std::endl;

        LZWBinFa f2;
        f2 << '0' << '1';
        std::cout << "f2" << std::endl << f2 << std::endl;

        LZWBinFa f3;
        f3 << '0' << '1' << '1' << '1' << '0';
        std::cout << "f3" << std::endl << f3 << std::endl;

        f1 = f2 = f3;
        
        std::cout << "f1" << std::endl << f1 << std::endl;
        std::cout << "f2" << std::endl << f2 << std::endl;
        std::cout << "f3" << std::endl << f3 << std::endl;
		
        /*
    	std::cout << "Az alap fa: " << std::endl << binFa << std::endl;

        LZWBinFa b1;
        b1 << '0' << '1' << '1' << '1' << '0';
        std::cout << "Egy tök új binfa: " << std::endl << b1 << std::endl;        

        std::cout << "operator= függvény hívódik " << std::endl;
        binFa = b1;

        std::cout << "Az alap fa immár felülírva: " << std::endl << binFa << std::endl;

        LZWBinFa bc(binFa);
        std::cout << "Egy másolat: " << std::endl << bc << std::endl;

        bc << '0' << '1' << '1' << '1' << '0';
        std::cout << "Módosítom a másolatot: << '0' << '1' << '1' << '1' << '0';" << std::endl << bc << std::endl;

        std::cout << "Az alap fa: " << std::endl << binFa << std::endl;
        
    }

    return 0;
}*/