#include <cmath>
#include "binfa.h"

// A következő függvény definícióknál a hatókör operátor előtti
// osztálynév azért kell, hogy a függvényekben az osztály tagadatait
// el lehessen érni szépen

/*
    hogyan shifteljük majd bele a fába a 0-kat vagy 1-eket
*/
LZWBinFa& LZWBinFa::operator<<(char b) 
{
    // nullát, vagy egyet kell betenni a fába?
    if (b == '0')
    {
        if (!fa -> getNullaGyermek())
        {
            // ha még nincs a csomóponton (lásd Csomopont* fa) semmi
            // akkor teszünk oda egyet
            Csomopont* uj = new Csomopont('0');
            fa -> setNullaGyermek(uj);
            fa = gyoker;

    		nullasok++;
        }
        else
        {
            // ha már van ott valami, akkor csak oda lépünk
            // a fa-val
            fa = fa -> getNullaGyermek();
        }
    }
    else
    {
        // itt minden hasonlóan az előző részhez, csak itt 1-est
        // teszünk a fába
        if (!fa -> getEgyGyermek())
        {
            Csomopont* uj = new Csomopont('1');
            fa -> setEgyGyermek(uj);
            fa = gyoker;

            egyesek++;
        }
        else
        {
            fa = fa -> getEgyGyermek();
        }
    }

    return *this;
}

int LZWBinFa::getMelyseg(void)
{
    melyseg = maxMelyseg = 0;
    rmelyseg(gyoker);
    return maxMelyseg - 1;
}

double LZWBinFa::getAtlag(void)
{
    melyseg = atlagosszeg = atlagdb = 0;
    ratlag(gyoker);

    atlag = ((double) atlagosszeg) / atlagdb;
    return atlag;
}

double LZWBinFa::getSzoras(void)
{
    atlag = getAtlag();
    szorasosszeg = 0.0;
    melyseg = atlagdb = 0;

    rszoras(gyoker);

    if (atlagdb - 1 > 0)
    {
        szoras = std::sqrt(szorasosszeg / (atlagdb - 1));
    }
    else
    {
        szoras = std::sqrt(szorasosszeg);
    }

    return szoras;
}

/*
    Ezek a függvények számítják ki a keresett dolgokat (pl.: mélység, stb.)

    Az rvalami függvénynévben az 'r' az elején a rekurzív szóra utal.
*/

void LZWBinFa::rmelyseg(Csomopont* elem)
{
    if (elem != NULL)
    {
        ++melyseg;

        if (melyseg > maxMelyseg)
        {
            maxMelyseg = melyseg;
        }

        rmelyseg(elem -> getEgyGyermek());
        rmelyseg(elem -> getNullaGyermek());

        --melyseg;
    }
}

void LZWBinFa::ratlag(Csomopont* elem)
{
    if (elem != NULL)
    {
        ++melyseg;

        ratlag(elem -> getEgyGyermek());
        ratlag(elem -> getNullaGyermek());

        --melyseg;

        if (elem -> getEgyGyermek() == NULL && elem -> getNullaGyermek() == NULL)
        {
            ++atlagdb;
            atlagosszeg += melyseg;
        }
    }
}

void LZWBinFa::rszoras (Csomopont* elem)
{
    if (elem != NULL)
    {
        ++melyseg;

        rszoras(elem -> getEgyGyermek());
        rszoras(elem -> getNullaGyermek());

        --melyseg;

        if (elem -> getEgyGyermek() == NULL && elem -> getNullaGyermek() == NULL)
        {
            ++atlagdb;
            szorasosszeg += ((melyseg - atlag) * (melyseg - atlag));
        }
    }
}