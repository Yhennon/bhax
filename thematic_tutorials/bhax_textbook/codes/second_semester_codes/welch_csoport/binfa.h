#ifndef LZWBinFa__H
#define LZWBinFa__H

#include <iostream>

class LZWBinFa
{
public:
	LZWBinFa():egyesek(0), nullasok(0)
	{
		gyoker = new Csomopont();
		fa = gyoker;

		std::cout << " -- Konstruktor: " << this << std::endl;
	}

	LZWBinFa(const LZWBinFa& forras):egyesek(forras.egyesek), nullasok(forras.nullasok)
	{
		gyoker = melyMasol(forras.gyoker, forras);
		fa = gyoker;
	}

	LZWBinFa(LZWBinFa&& jobbertek)
	{
		std::cout<<"meghivodott a mozgató const"<<std::endl;
		gyoker = nullptr;
		/*
			gyoker.ujEgyesGyermek(nullptr);
			gyoker.ujNullasGyermek(nullptr);
		*/

		// ez az std::move azért kell, mert amit megkapott std::move(F)-et a példában
		// az itt már balérték-ként fog szerepelni

		*this = std::move(jobbertek);

		std::cout << " -- Mozgató konstruktor: " << this << std::endl;
		jobbertek.~LZWBinFa();
	}

	LZWBinFa& operator= (LZWBinFa&& jobbertek)
	{
		std::cout << "meghivodott mozgató ertek"<<std::endl;
		//gyoker = nullptr;
		std::swap(jobbertek.gyoker, gyoker);

		std::cout << " -- Mozgató értékadás: " << this << std::endl;
		return *this;
	}

	LZWBinFa& operator= (const LZWBinFa& forras)
	{
		std::cout << "MÁSOLÓ ÉRTÉKADÁS!!" << std::endl;
		if (this == &forras)
		{
			return *this;
		}
		
		egyesek = forras.egyesek;
		nullasok = forras.nullasok;

		szabadit(gyoker);
		gyoker = melyMasol(forras.gyoker, forras);
		
		
		return *this;
	}

	~LZWBinFa()
	{
		szabadit(gyoker);
		std::cout << " -- Destruktor: " << this << std::endl;
	}

	/*
		Hogyan shifteljük majd bele a fába a 0-kat vagy 1-eket
		(akár láncolva is)
	*/
	LZWBinFa& operator<<(char b);
	
	/*
		Elegánsabb megoldás, egy public kiir függvény, mely segítségével
		el tudjuk érni azt, hogy ha ezt hívjuk meg kívülről ne legyen semmi
		baj a mélységgel (mert hogy az amúgy privát), mindig a gyökérttől írjuk
		ki a fát, a tényleges tagadatot csak az osztály használja igazán
	*/
	void kiir(void)
	{
		melyseg = 0;
		kiir(gyoker, std::cout);
	}

	/*
		túlterhelem az előbb megírt (void) paraméterű kiir függvényt, hogy 
	    tudjak más streamre is írni, ne csak std::cout-ra, ez azért fontos,
	    hogy lehessen így is kiírni binfákat:
	
	    std::cout << fa1 << fa2 << fa3 << endl;

	    melyet igazán a következő függvény használ
	*/
	void kiir(std::ostream& os)
	{
		melyseg = 0;
		kiir(gyoker, os);
	}

	/*
		a << operátor definiálása, hogy lehessen így is kiírni binfákat:
	
		std::cout << fa1 << fa2 << fa3 << endl;
	*/
	friend std::ostream& operator<<(std::ostream& os, LZWBinFa& bf)
	{
		bf.kiir(os);
		return os;
	}

	/*
		a fában található egyesek illetve nullák
	*/
	int getEgyesek(void) const
	{
		return egyesek;
	}

	int getNullasok(void) const
	{
		return nullasok;
	}

	/*
		a mélységet, átlagot, szórást adják meg,
		a protected függvényekkel, melyek kb olyanok, mint a privát kiir
	*/
	int getMelyseg(void);

	double getAtlag(void);

	double getSzoras(void);

	private:
		class Csomopont
		{
		public:
			/*
				Konstruktor egy csomópont létrehozásához

				alapértelmezetten, ha nem mondunk semmit, a gyökér csomópontot
				hozzuk létre / értékkel és két NULL (0) ággal
			*/
			Csomopont(char b = '/'):betu(b), balNulla(0), jobbEgy(0)
			{
			}

			~Csomopont()
			{
			};

			/* GETTEREK (nem bántjuk a privát dolgokat, csak rákérdezünk mi az)*/

			Csomopont* getNullaGyermek() const
			{
				return balNulla;
			}

			Csomopont* getEgyGyermek() const
			{
				return jobbEgy;
			}

			char getBetu() const
			{
				return betu;
			}

			/* SETTEREK (legyen a te ágad egy gy csomópont)*/

			void setBetu(char b)
			{
				betu = b;
			}

			void setNullaGyermek(Csomopont* gy)
			{
				balNulla = gy;
			}

			void setEgyGyermek(Csomopont* gy)
			{
				jobbEgy = gy;
			}
		private:
			char betu;           // milyen betű is ő

			Csomopont* balNulla; // egyik ág
			Csomopont* jobbEgy;  // másik ág

			// a másolást nem itt oldom meg!
			// van cucc* a memóriában, le kell tiltani a másoló dolgokat
			Csomopont(const Csomopont&);
			Csomopont& operator=(const Csomopont&);
		};
	public:
		Csomopont* getGyoker(void) const
		{
			return gyoker;
		}
	private:
		Csomopont* fa;                     // aktuális csomópontra mutat

		int melyseg, atlagosszeg, atlagdb; //
		double szorasosszeg;               // getMelyseg() stb-hez az értékek

		/*
		    inorder bejárás és kiiratás
		    kiírjuk os-re az adott csomópontot egy szép forral
		*/
		void kiir(Csomopont* elem, std::ostream& os)
		{
		    // ez kell, hogy ne legyen "Szegmentálási hiba", meg
		    // ettől az iftől áll meg a rekurzió
		    if (elem != NULL)
		    {
		        ++melyseg;
		        kiir(elem -> getEgyGyermek(), os);

		        for (int i = 0; i < melyseg; ++i)
		        {
		            os << "---";
		        }

		        os << elem -> getBetu() << "(" << melyseg - 1 << ")" << std::endl;

		        kiir(elem -> getNullaGyermek(), os);
		        --melyseg;
		    }
		}

		/*
			Rekurzív másolás, új memória foglalásokkal
			Védési feladat:

			megy a program ahogy eddig
			amikor kész legyen egy vektor, abba tegyük bele a fát, de úgy, hogy egy mélymásoló konstruktorral
		*/
		Csomopont* melyMasol(Csomopont* forras, const LZWBinFa& forrasFa)
		{
			if (forras == NULL)
			{
				return NULL;
			}

			Csomopont* ujCsomo = new Csomopont(forras -> getBetu());

			ujCsomo -> setEgyGyermek(melyMasol(forras -> getEgyGyermek(), forrasFa));

			if (forrasFa.fa == forras)
			{
				fa = ujCsomo;
			}

			ujCsomo -> setNullaGyermek(melyMasol(forras -> getNullaGyermek(), forrasFa));

			return ujCsomo;
		}

		void szabadit(Csomopont* elem)
		{
			if (elem != NULL)
			{
				szabadit(elem -> getEgyGyermek());
				szabadit(elem -> getNullaGyermek());

				// ha a csomópont mindkét gyermekét felszabadítottuk
				// azután szabadítjuk magát a csomópontot:
				delete elem;
			}
		}

protected:		
	// ha esetleg egyszer majd kiterjesztjük az osztályt, mert
	// akarunk benne valami újdonságot csinálni, vagy meglévő tevékenységet máshogy... stb.
	// akkor ezek látszanak majd a gyerek osztályban is

	Csomopont* gyoker;     // gyökér
	int maxMelyseg;    
	int egyesek, nullasok;   
	double atlag, szoras;

	/*
		ezeket sem itt definiálom, hasonlóak lesznek, mint a kiir

		r --> rekurzív
	*/
	void rmelyseg(Csomopont* elem);

	void ratlag(Csomopont* elem);
	
	void rszoras(Csomopont* elem);

};
#endif