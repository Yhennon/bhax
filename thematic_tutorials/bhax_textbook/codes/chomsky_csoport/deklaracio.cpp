#include <iostream>

using namespace std;

int* fuggveny()
{
	int valami;
	int *egesz=&valami;
	return egesz;
}
//=============================
int fuggveny2(int a, int b)
{
	return a+b; 
}

typedef int (*fuggvenymutato)(int, int);

fuggvenymutato fuggveny3(int a)
{

	return fuggveny2;
}

int main()
{
	//egész
	int egesz = 10;
	//egészre mutató mutató
	int *emMutato = &egesz;
	//egész referenciája
	int &referencia = egesz;
	//egészek tömbje
	int tomb[3] = {5,2,1};
	//egészek tömbjének referenciája (nem az első elemé)
	int (&tombreferencia)[3] = tomb;
	//egészre mutató mutatók tömbje
	int *emMtomb[3];
	//egészre mutató mutatót visszaadó függvény
	//lasd main folott
	//egészekre mutató mutatót visszaadó függvényre mutató mutató
	int* (*fcptr)() = fuggveny;
	fuggvenymutato (*fcptr2)(int) = fuggveny3;

	cout << egesz <<"\n"<<emMutato<<"\n"<<tomb<<"\n"<<tombreferencia<<"\n"<<emMtomb<<"\n"<<fuggveny()<<endl
	<<endl<<fcptr();
	cout<<endl<<fuggveny3(3)(4,1);
	cout<<endl<<fcptr2(4)(3,2);


}