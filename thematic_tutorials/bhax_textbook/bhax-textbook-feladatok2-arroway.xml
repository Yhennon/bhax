<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Arroway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>OO szemlélet</title>
        <para>
            Feladat:
            A módosított polártranszformációs normális generátor beprogramozása Java nyelven. Mutassunk rá,
            hogy a mi természetes saját megoldásunk (az algoritmus egyszerre két normálist állít elő, kell egy
            példánytag, amely a nem visszaadottat tárolja és egy logikai tag, hogy van-e tárolt vagy futtatni kell
            az algot.) és az OpenJDK, Oracle JDK-ban a Sun által adott OO szervezés ua.!
            
            Ugyanezt írjuk meg C++ nyelven is!
        </para>
        <para>
            Source :  
            <link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/PolarGenerator.java">
                <filename>PolarGenerator.java</filename>
            </link>   
                 
             <link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/polargenerator.cpp">
                <filename>polargenerator.cpp</filename>
            </link>                  
        </para>
        <para>
            A feladat egyszer már szóba került a könyv írása során, most Javaban és c++ -ban is megírjuk a kódot.
        </para>
        <para>
            Létrehozunk egy <classname>PolarGenerator</classname> nevű osztályt, és abban két változót.
            Az egyik egy logikai változó, a <varname>nincsTarolt</varname>.
            Ezt a változót majd arra használjuk fel, hogy megnézzük, van e éppen tárolt számunk, vagy nincs, és igaz értékkel
            inicializáljuk, természetesen tehát a kezdésnél nincsen semmilyen eltárolt értékünk.
            Deklaráljuk a másik változót, egy double-t, a neve <varname>tarolt</varname>. Ebben egy törtszámot fogunk tárolni, 
            amit a <function>kovetkezo()</function> függvényben számoltatunk ki.
            <programlisting language="java">
                <![CDATA[
public class PolarGenerator {                
    boolean nincsTarolt = true;
    double tarolt;
]]>
            </programlisting>
        </para>
        <para>
            Készítünk egy konstruktort a <classname>PolarGenerator</classname> osztályunknak.
            
            <programlisting language="java">
                <![CDATA[
public PolarGenerator() {

}
]]>
            </programlisting>
        </para>
        <para>   
            Nézzük is a <function>kovetkezo()</function> függvényt.
            Kezdésnek megnézzük, hogy van e eltárolt számunk. Ha nincs, azaz ha a <varname>nincsTarolt</varname> értéke igaz, 
            akkor deklarálunk 5 új változót, mindegyiket double típussal, ezeknek a nevei u1, u2, v1, v2, w.
            Ezután definiáljuk őket, értéket adunk nekik. Az u1 és u2 változónak véletlenszerűen szeretnénk értéket adni,
            ehhez a <methodname>Math.random()</methodname> metódust használjuk fel, ami egy pszeudo-random(hamis) valós számot fog generál, ami
            nullánál nagyobb vagy egyenlő, és egynél kisebb.
            Most viszont tényleges, igazi véletlenszám generátort akarunk létrehozni.
            Ehhez még a következő matematikai műveleteket hajtjuk végre:  
            <programlisting language="java">
                <![CDATA[
public double kovetkezo() {
    if(nincsTarolt) {
	double u1, u2, v1, v2, w;
	do {
	u1 = Math.random();
        u2 = Math.random();
	v1 = 2 * u1 - 1;
	v2 = 2 * u2 - 1;
	w = v1 * v1 + v2 * v2;
	} while (w > 1);
		double r = Math.sqrt((-2 * Math.log(w)) / w);
		tarolt = r * v2;
		nincsTarolt = !nincsTarolt;
		return r * v1;
	} else {
		nincsTarolt = !nincsTarolt;
		return tarolt;
        }
}
]]>
            </programlisting>
            Így -1(nyílt) és 1(zárt) között generálunk valós véletlen számokat.
        </para>
        <para>
            Minden esetben, akár nem volt tárolt számunk, akár volt, a feltétel vizsgálata és kód lefuttatása után a <varname>nincsTarolt</varname>
            értékét az ellentettjére állítjuk.
        </para>
        <para>
            Végül a program main részében példányosítjuk a <classname>PolarGenerator</classname> osztályunkat, és tízszer kiiratjuk a
            <function>kovetkezo()</function> eredményét.
        </para>
        <para>
            A Sun JDK-jában láthatjuk hogy ugyanez a módszer van megírva, ez a <function>nextGaussian()</function> függvény.
            Van viszont egy eltérés, az, hogy <function>StrictMath.sqrt()</function>-t és <function>StrictMath.log()</function>-ot használnak.
            A StrictMath osztályt alkalmazva, bármilyen rendszert használva is, mindig bitről bitre megegyező értéket fogunk kapni egy adott
            művelet elvégzésének eredményeképpen, tehát a JDK polárgenerátorát használva precízebb eredményeket kaphatunk.
        </para>
        <para>
            Ezen felül azt vehetjük még észre, hogy ez egy synchronized metódus.
            Szinkronizációra azért van szükség, hogy megakadályozzuk, hogy ezt az erőforrást egyszerre több thread is
            használni próbálja, és így inkonzisztens eredményeket kapjunk. 
            A szinkronizálásra a synchronized kulcssszót használva, synchronized blokkot hoznak létre.
            Egy ilyen blokkot egyszerre csak egy thread tud használni egy időben, ha éppen használatban van, és más is használni
            szeretné, meg kell várnia amíg újra szabad nem lesz.
            Az objektumoknak van egy lock-ja, egy zárja, amit egy threadnek meg kell szereznie, hogy használhassa, majd 
            ha már nem használja, "elengedi" a zárat,és egy másik thread használhatja, ha kell.
            A zárak implementálást a Java 5 óta a java.util.concurrent.locks kezeli.
        </para>
        <para>
            <figure>
            <title>nextGaussian() a jdk-ban</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="kepek/randomsun.jpg" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>   
        </para>
        
    </section>        

    <section>
        <title>Homokozó</title>
        <para>
            Feladat:
            Írjuk át az első védési programot (LZW binfa) C++ nyelvről Java nyelvre, ugyanúgy működjön!
            Mutassunk rá, hogy gyakorlatilag a pointereket és referenciákat kell kiirtani és minden máris működik
            (erre utal a feladat neve, hogy Java-ban minden referencia, nincs választás, hogy mondjuk egy
            attribútum pointer, referencia vagy tagként tartalmazott legyen).
            Miután már áttettük Java nyelvre, tegyük be egy Java Servletbe és a böngészőből GET-es kéréssel
            (például a böngésző címsorából) kapja meg azt a mintát, amelynek kiszámolja az LZW binfáját!
        </para>
        <para>
            Források:<link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/BinaryTree.java">
                <filename>BinaryTree.java</filename>
            </link>
            <link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/Main.java">
                <filename>Main.java</filename>
            </link>
            
            Ahogy a feladat is mondja, és ahogy az olvasónaplóban leírtuk, Java nyelvben nem használunk mutatókat, mert minden 
            egy referencia. A megadott c++ os binfa forrást átalakítjuk, töröljük a mutatókat,referenciákat, és Java szintaxist használunk.
        </para>
        <para>
            C++-os minta,mutatók,gyökerek vannak(*, &amp;)
            <programlisting language="c++">
                <![CDATA[
            ..
            ..
            ..
        else {
            if (!fa->egyesGyermek()) {
                Csomopont* uj = new Csomopont('1');
                fa->ujEgyesGyermek(uj);
                fa = &gyoker;
            }
            else {
                fa = fa->egyesGyermek();
            }
            ..
            ..
]]>
            </programlisting>
            Java-s minta:
            <programlisting language="java">
                <![CDATA[
        if (newItem == '1') {
            if (current.right() == null) {
                current.setRight(new Node('1'));
                setCurrent(root);
            } else {
                setCurrent(current.right());
            }
        }
]]>
            </programlisting>
        </para>
        <para>
            Ezután letöltjük az Apache Tomcat-et a weboldaláról, aminek segítségével létrehozunk egy szervletet, ami a Binfát fogja használni
            a majdani böngészős kérésünkhöz, a címsorba fogjuk a bináris számsorozatot írni az eddigi infile helyett.
        </para>
        <para>
            A tomcat webapps mappájában készítettem egy Binfa nevű mappát, amibe behelyeztem a 2 forráskódomat, ezt
            fogja majd futtatni a servlet a kéréseknél. A Main fájlban írjuk meg a servletkészítéshez szükséges dolgokat, és a <function>doGet()</function>
            függvényben leírjuk hogy hogyan kell megadni a számsort és hogy hogyan fogja ezt kezelni(a BinaryTree forráskód értékeit használva).
            <programlisting language="java">
                <![CDATA[
                    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        data = request.getParameter("data");
        tree = new BinaryTree();
        response.setContentType("text/html;charset=UTF-8");
        for (int i = 0; i < data.length(); i++) {
            tree.addItem(data.charAt(i));
        }
        PrintWriter out = response.getWriter();
        try {
            out.println("<!DOCTYPE html>");
            out.println("<html><head>");
            out.println("<meta http-eqiuv='Content-Type' content ='text/html; charset= UTF-8'>");
            out.println("<body>");
            tree.writeOut(tree.getRoot(), out);
            out.println("<br></br>");
            out.println("<p>Elemszam atlaga " + tree.getElemszamAtlag() + "</p>");
            out.println("<p>Atlag " + tree.getAtlag() + "</p>");
            out.println("<p>Melyseg " + tree.getMelyseg() + "</p>");
            out.println("<p>Szoras " + tree.getSzoras() + "</p>");
            out.println("</body>");
            out.println("</html>");
]]>
            </programlisting>   
            Ehhez szükség van még a Binfa mappában elhelyezni egy xml fájlt  ,ebben megadjuk a servlet nevét, és azt hogy egy get kérést
            használhatunk.
            <programlisting language="java">
                <![CDATA[
         <?xml version="1.0" encoding="ISO-8859-1"?>
<web-app version="3.0"
    xmlns="http://java.sun.com/xml/ns/javaee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd">
    
    <!-- To save as <CATALINA_HOME>\webapps\helloservlet\WEB-INF\web.xml -->
    
    <servlet>
        <servlet-name>Binfa</servlet-name>
        <servlet-class>com.company.Main</servlet-class>
    </servlet>
    
    <!-- Note: All <servlet> elements MUST be grouped together and
         placed IN FRONT of the <servlet-mapping> elements -->
    
    <servlet-mapping>
        <servlet-name>Binfa</servlet-name>
        <url-pattern>/get</url-pattern>
    </servlet-mapping>
    
</web-app>
]]>
            </programlisting>           
        </para>
        <para>
            A tomcat bin könyvtárában találhatjuk (több más között) a startup.sh fájlt, amit futtatunk hogy elindítsuk szervert:
        <figure>
            <title>tomcat servlet indítás</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="kepek/tomcat_servlet.jpg" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>   
        
        A shutdown.sh-val leállíthatjuk a servletet.
        </para>
        <para>
            Végül pedig elérhetjük a servletet a localhost:8080 címén, a Binfa funkcióját tudjuk használni, és ahogy az ehhez tartozó
            xml ben leírtuk, get kérést tudunk használni a Main-ben leírt módon, így :
            localhost:8080/Binfa/get?data=1010101001011111100. A data= után írjuk be a bináris sorozatot, és megkapjuk az eredményt.
            <figure>
            <title>az eredmény</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="kepek/tomcat_output.png" scale="30" />
                </imageobject>
            </mediaobject>
        </figure>   
        </para>
        
    </section>    
        
    <section>
        <title>Gagyi</title>
        <para>
        Feladat: Az ismert formális „while (x &lt;= t &amp;&amp; x &gt;= t &amp;&amp; t != x);” tesztkérdéstípusra adj a szokásosnál
        (miszerint x, t az egyik esetben az objektum által hordozott érték, a másikban meg az objektum
        referenciája) „mélyebb” választ, írj Java példaprogramot mely egyszer végtelen ciklus, más x, t
        értékekkel meg nem! A példát építsd a JDK Integer.java forrására 3 , hogy a 128-nál inkluzív objektum
        példányokat poolozza!
        </para>
        <para>
            Source:    
             <link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/gagyi.java">
                <filename>gagyi.java</filename>
            </link>              
        </para>
        <para>
        A feladat egy felmerült tesztkérdés továbbgondolása, illetve pontos válaszadás a kérdésre.
        Adott egy <function>while(x &lt;= t &amp;&amp; x &gt;= t &amp;&amp; t != x)</function> ciklus, és két értékpár,egyszer 
        <varname>x</varname> = -128 ,<varname>t</varname> = -128, majd <varname>x</varname> = -129 ,<varname>t</varname> = -129 .
        Ami érdekes, az az, hogy -128-as értékekkel a <function>while</function> ciklust használva és az értékeket kiíratva
        a két érték megjelenik, -129-es értékeket használva pedig végtelen ciklust kapunk.
        </para>
        <para>-128-al:
        </para>
        <screen>
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ java gagyi
-128
-128
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ 
        </screen>
        <para>
            -129-el: 
        </para>
        <screen>
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ javac gagyi.java
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ java gagyi
^Cyhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$      
        </screen>
        <para>
          A különbség megértéséhez az Integer.java forrását kell megvizsgálnunk és egy részét értelmeznünk:
          <link xlink:href="https://hg.openjdk.java.net/jdk/jdk11/file/1ddf9a99e4ad/src/java.base/share/classes/java/lang/Integer.java?fbclid=IwAR2KBKQc63izndqEBFDY4cKpUdP4RByqNyAzZnzXmM1TtYBsoFr6NYbyFng#l997
">https://hg.openjdk.java.net/jdk/jdk11/file/1ddf9a99e4ad/src/java.base/share/classes/java/lang/Integer.java?fbclid=IwAR2KBKQc63izndqEBFDY4cKpUdP4RByqNyAzZnzXmM1TtYBsoFr6NYbyFng#l997
</link>   
        </para>
        <para>
            A forrásban meg van írva, hogy egy gyorsítótárat készítsenek az IntegerCache osztály részeként, ami elősegíti az autoboxolását a -128 és 127 közötti int
            értkékeknek. Egy határt is beállít az osztályban a primitív értékeknek, amiket autoboxolhat, ez -128 tól egy konfigurálható
            értékig(<varname>high</varname>-ig) terjed. Az autoboxolás például az, amikor a compiler egy  int típusú primitív értéket egy Integer wrapper-osztálybeli
            objektummá alakít. A Java compiler a következő esetekben alkalmazza az autoboxolást:
            <itemizedlist mark='opencircle'>
                <listitem>
                    <para>
                     Amikor a primitív érték egy olyan függvény paramétereként van megadva, ami egy (a primitív valueval) megegyező
                     wrapper-osztálybeli objektumot vár.
                    </para>
                </listitem>
                <listitem>
                    <para>
                     Amikor a primitív érték a vele megegyező wrapper-osztálybeli változóhoz van hozzárendelve.
                    </para>
                </listitem>
            </itemizedlist>
            Ennek a visszafele történő alkalmazása az unboxing.
            <programlisting language="java">
                <![CDATA[

            cache = new Integer[(high - low) + 1];

            int j = low;

            for(int k = 0; k < cache.length; k++)

                cache[k] = new Integer(j++);


            // range [-128, 127] must be interned (JLS7 5.1.7)

            assert IntegerCache.high >= 127;
]]>
            </programlisting>            
           
        </para>
        <para>
            Amikor egy új <type>int</type>-et példányosítunk, akkor lefut a következő rész:
            <programlisting language="java">
                <![CDATA[
public static Integer valueOf(int i) {

        if (i >= IntegerCache.low && i <= IntegerCache.high)

            return IntegerCache.cache[i + (-IntegerCache.low)];

        return new Integer(i);

    }
]]>
            </programlisting>  
            Megnézi a program, hogy az új int értéke a <varname>low</varname> és <varname>high</varname> között van e, 
            amennyiben igen, az IntegerCache-ből indexeli, és az int referenciája az lesz, amit a cache az első lefuttatáskor
            történő inicializáláskor kapott. Amennyiben pedig ezen a rangen kívül adunk értéket az int változónak, akkor az egy új memóriacímre fog
            referálni.
        </para>
        <para>
            
        </para>
        <para>
            <programlisting language="java">
                <![CDATA[
     * The cache is initialized on first usage. 
..
..                
     * Returns an {@code Integer} instance representing the specified

     * {@code int} value. 
..
..
     * This method will always cache values in the range -128 to 127,

     * inclusive, and may cache other values outside of this range.
]]>
            </programlisting>              
            Így tehát, mivel az első esetben -128at megadva az előre létrehozott rangen belül választottunk értéket, a 
            <function>while(x &lt;= t &amp;&amp; x &gt;= t &amp;&amp; t != x)</function> ciklusnak nem teljesül az utolsó
            feltétele mert mind <varname>t</varname> és mind <varname>x</varname> ugyanannak az objektumnak a referenciáját hordozzák.
        </para>
        <para>
            Amikor pedig -129 et használunk, az összes feltétel teljesül, és sosem lépünk ki a while-ból, így végtelen ciklushoz jutunk.
        </para>
    </section> 
    <section>
        <title>Yoda</title>
        <para>
            Feladat: Írjunk olyan Java programot, ami java.lang.NullPointerEx-el leáll, ha nem követjük a Yoda conditions-t!
            <link xlink:href="https://en.wikipedia.org/wiki/Yoda_conditions">https://en.wikipedia.org/wiki/Yoda_conditions</link>
        </para>
        <para>
            Ha valaki ismeri a Star Wars univerzumát, az biztosan ismeri és hallota már beszélni Yoda mestert.
            Yoda egy nem megszokott sorrendben alkotja meg a mondatokat, eltér az általunk használt Subject-Verb-Object mondatalkotástól,
            és Object-Subject-Verb -et használ. Például, magyarul egy helyes mondat úgy hangzik, hogy :'Én szeretek zenét hallgatni'.
            Ha Yoda mestert utánozva mondanám ugyanezt a mondatot, a Yoda conditions-t használva, akkor pedig így hangzik a mondat:
            'Zenét hallgatni Én szeretek'. 
            A feladatunk most pedig az, hogy olyan programot írjunk, ami működik, ha tartjuk magunkat a Yoda feltételekhez,és ha nem felelünk
            meg ennek, akkor NullPointerException-nel álljon le.
        </para>
        <para>
            Javaban lehet használni egy speciális <literal>null</literal> értéket különböző célokra, főleg arra, hogy megmondjuk egy
            változóról, hogy nincs hozzárendelve érték.
            NullPointerException akkor fordul elő, amikor a program olyan objektum referenciát akar használni, aminek
            az értkéke <literal>null</literal>.
        </para>
        <para>
            Source: 
            <link xlink:href="../../../bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway/yoda.java">
                <filename>yoda.java</filename>
            </link>               
        </para>
        <para>
            Legyen egy egyszerű <function>if()</function> függvényünk, amiben két string értékéről nézzük meg, hogy egyenlőek e.
            (ezt a stringek <function>equals()</function> metódusával tesszük)
            Az egyik string értéke legyen <literal>null</literal> (tehát semmit nem rendelünk hozzá!), a másiké egy általunk választott bármilyen szó, most a tubeOfCheese.
            Az, hogy mit csináljon a program igaz feltétel esetén, most lényegtelen.(De egy egyszerű kiiratással tesztelhető;
            ha igaz, írjuk is ki, ha pedig hamis, akkor valami mást írjunk.)
            Megszokott módon így néz ki a feltétel:
        </para>
        <para>
            <programlisting language="java">
                <![CDATA[
if (wordOne.equals(wordTwo)){
...
}
]]>
            </programlisting> 
        </para>
        <para> 
            Lefuttatva a programunkat ezt a feltételt használva, ezt láthatjuk:        
            <screen>
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ java Main
Exception in thread "main" java.lang.NullPointerException
	at Main.main(yoda.java:9)
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ 
            </screen>
        </para>
        <para>
            Nézzük, mi is történik.Tehát, a <varname>wordOne</varname> stringhez nem rendeltünk értéket, csak a speciális
           <literal>null</literal>-t, ami ezt indikálja, a <varname>wordTwo</varname> -hez pedig a "tubeOfCheese"-t.
            Mivel a <varname>wordOne</varname> hoz nem lett semmi hozzárendelve,ezért nincs referenciája, nincs amit össze lehetne hasonlítani 
            mással, ezért kapunk NullPointerExceptiont futtatáskor.
        </para>
        <para>
            Ha fordítva írjuk a feltételt, azaz a második szót hasonlítjuk az elsőhöz, így eleget téve a yoda feltételnek, akkor pedig
            a kódot lefuttatva ezt látjuk:
        </para>
        <para>
            <programlisting language="java">
                <![CDATA[
if (wordTwo.equals(wordOne)){	
		System.out.println("They are equals.");	
	}else{
		System.out.println("sziasztok");
	}
]]>
            </programlisting> 
        </para>        
        <para>
            Ebben az esetben már a <varname>wordTwo</varname> létező értékét hasonlítjuk a <varname>wordOne</varname> -éhoz, amihez nincs
            hozzárendelve semmi, így nem egyenlőek, és az if függvény hamis ága fog lefutni.
        </para>
        <screen>
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$ java Main
sziasztok
yhennnon@Yhennon-vm:~/naakkormost/bhax/thematic_tutorials/bhax_textbook/codes/third_semester_codes/1_arroway$             
        </screen>
    </section>                                  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
